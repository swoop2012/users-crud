package com.userscrud.controller;

import com.userscrud.model.User;
import com.userscrud.model.UserSearch;
import com.userscrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value = "/users")
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    HttpEntity<PagedResources<User>> persons(@ModelAttribute("user") UserSearch user, Pageable pageable,
                                               PagedResourcesAssembler assembler) {
        Page<User> users = userService.listUsers(user, pageable);
        @SuppressWarnings("unchecked")
        PagedResources<User> pagedResources = assembler.toResource(users);
        return  new ResponseEntity<PagedResources<User>>(pagedResources, HttpStatus.OK);
    }

}