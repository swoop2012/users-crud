package com.userscrud.specification;

import com.userscrud.model.User;
import com.userscrud.model.UserSearch;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.text.SimpleDateFormat;
import java.util.*;


public class UserSpecs {

    public static Specification<User> getSearchCriteria(final UserSearch model) {
        return new Specification<User>() {
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {

                List<Predicate> predicates = new ArrayList<Predicate>();
                try {
                    BeanInfo beanInfo = Introspector.getBeanInfo(UserSearch.class);
                    if(beanInfo != null)
                    for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                        if(propertyDesc == null)
                            continue;
                        String propertyName = propertyDesc.getName();
                        Object value = propertyDesc.getReadMethod().invoke(model);
                        if(propertyName != null && !propertyName.equals("class") && value != null && !value.equals(""))
                        {
                            String stringValue = value.toString();
                            if(stringValue == null || stringValue.isEmpty())
                                continue;
                            if(propertyName.equals("name"))
                                predicates.add(builder.like(root.<String> get(propertyName), '%' + stringValue + '%'));
                            else if(propertyName.contains("Date")) {
                                Calendar dateCalendar = Calendar.getInstance();
                                SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date date = parser.parse(stringValue);
                                dateCalendar.setTime(date);

                                Predicate timelessPredicate = builder.and(
                                        builder.equal(builder.function("year", Integer.class,root.<String>get(propertyName)), dateCalendar.get(Calendar.YEAR)),
                                        builder.equal(builder.function("month", Integer.class,root.<String>get(propertyName)), dateCalendar.get(Calendar.MONTH) + 1),
                                        builder.equal(builder.function("day", Integer.class,root.<String>get(propertyName)), dateCalendar.get(Calendar.DATE)));
                                predicates.add(timelessPredicate);
                            }
                            else {
                                predicates.add(builder.equal(root.<String>get(propertyName), stringValue));
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}

