package com.userscrud.init;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Created by User on 24.12.2014.
 */
@Configuration
@Import(RepositoryRestMvcConfiguration.class)
public class RestConfig extends RepositoryRestMvcConfiguration {

}