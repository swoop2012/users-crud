package com.userscrud.init;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{JpaConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebAppConfig.class
                ,RestConfig.class
        };
    }

    @Override
    protected String getServletName() {
        return " ServletName";
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}