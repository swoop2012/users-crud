(function(){
	var usersService = function($http, $filter) {
		this.getUsers = function(model, options){
            options.currentPage--;
            var params = angular.extend({},model, {page:options.currentPage, size:options.pageSize});
            if(options.sortBy != ''){
                params.sort = options.sortBy;
                params.sort += ',' + (options.reverse ? 'desc':'asc');
            }
            if(params.hasOwnProperty('createdDate') && typeof params.createdDate == 'object')
                params.createdDate = $filter('date')(params.createdDate, 'yyyy-MM-dd');
            return $http.get(urlBase + '/users/?'+serializeData(params));
		};
        this.getUser = function(id){
            return $http.get(urlBase + '/users-crud/' + id);
        };
        this.createUser = function(model){
            return $http.post(urlBase + '/users-crud', model);
        };
        this.updateUser = function(id, model){
            return $http.patch(urlBase  + '/users-crud/' + id, model);
        };
        this.deleteUser = function (id){
            return $http.delete(urlBase + '/users-crud/' + id);
        };
	};
    usersService.$inject = ['$http', '$filter'];
	angular.module('usersApp').service('usersService', usersService);
}());