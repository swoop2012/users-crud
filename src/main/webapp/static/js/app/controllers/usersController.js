(function(){
	var usersController = function($scope, $log, $modal, usersService) {
        $scope.users = [];
        $scope.currentId = 0;
        $scope.userForm = {};
        $scope.searchUserForm = {};
        $scope.page = {};
        $scope.currentPage = 1;
        $scope.displayPages = 5;
        $scope.isRefreshing = false;
        $scope.sortBy = '';
        $scope.reverse = false;
        $scope.pageSize = 10;
        $scope.isAdminOptions = [{name:'Да',value:1},{name:'Нет',value:0},{name:'Все',value:''}];
        $scope.pageSizeOptions = [10,20,30,40,50];
        $scope.refresh = function () {
            $scope.isRefreshing = true;
            usersService.getUsers($scope.searchUserForm, {currentPage:$scope.currentPage, pageSize:$scope.pageSize, sortBy:$scope.sortBy, reverse:$scope.reverse})
				.success(function(data){
                    if(typeof data != "undefined" && data.hasOwnProperty('content'))
                        $scope.users = data.content;
                    $scope.page = data.page;
                    $scope.currentPage = $scope.page.number + 1;
                    $scope.isRefreshing = false;
				})
				.error(function(data, status){
					$log.log(data.error + ' ' + status);
                    $scope.isRefreshing = false;
				});
		};
        $scope.doSort = function(name){
            if($scope.sortBy == name)
                $scope.reverse = !$scope.reverse;
            else{
                $scope.sortBy = name;
                $scope.reverse = false;
            }
            $scope.refresh();
        };
        $scope.getSortClass = function(name){
            if($scope.sortBy == name)
                return $scope.reverse ? 'desc' : 'asc';
            return '';
        };
        $scope.checkRefreshNeeds = function()
        {
            if($scope.currentPage <= Math.ceil($scope.page.totalElements / $scope.pageSize))//trigger refresh only if
            // current page exists in new page range, otherwise pagination widget will trigger refresh
                $scope.refresh();
        };
        $scope.openDatePicker = function($event){
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.refresh();
        $scope.deleteUser = function(userId) {
            var modalInstance = $modal.open({
                templateUrl: 'confirmDeleteContent.html',
                controller: 'confirmDeleteController',
                size: '',
                resolve: {
                    userId: function () {
                        return userId;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            });
		};

        $scope.open = function (userId) {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'modalController',
                size: '',
                resolve: {
                    userId: function () {
                        return userId;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            });
        };
	};
    usersController.$inject = ['$scope', '$log', '$modal', 'usersService'];
	angular.module('usersApp').controller('usersController', usersController);
}());