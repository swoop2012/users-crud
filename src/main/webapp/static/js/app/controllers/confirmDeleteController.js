(function(){
    var confirmDeleteController = function ($scope,$log, $modalInstance, usersService, userId) {
        $scope.userId = userId;
        $scope.saving = false;
        $scope.ok = function() {
            $scope.saving = true;
            var promise;
            if($scope.userId > 0)
                promise = usersService.deleteUser($scope.userId).error(function(data, status){
                    $log.log(data.error + ' ' + status);
                }).success(function(){
                    $scope.saving = false;
                });
            $modalInstance.close(promise);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };
    confirmDeleteController.$inject = ['$scope', '$log', '$modalInstance', 'usersService', 'userId'];
    angular.module('usersApp').controller('confirmDeleteController', confirmDeleteController);
}());
