(function(){
    var modalController = function ($scope,$log, $modalInstance, usersService, userId) {
        $scope.userId = userId;
        $scope.userForm = {};
        var isNewRecord = true;
        $scope.saving = false;
        $scope.needValidation = false;
        $scope.showUserErrors = function(userForm, fieldName)
        {
            if(userForm.hasOwnProperty(fieldName))
                return ($scope.needValidation || userForm[fieldName].$dirty) && userForm[fieldName].$invalid;
            else
                return false;
        };
        if(typeof userId != 'undefined' && userId > 0)
        {
            usersService.getUser(userId).success(function(data){
                $scope.userForm = data;
            })
            .error(function(data, status){
                $log.log(data.error + ' ' + status);
            });
            isNewRecord = false;
        }
        $scope.ok = function(user) {
            if($scope.saving)
                return;
            $scope.needValidation = true;
            if(user.$valid)
            {
                var promise = isNewRecord ? usersService.createUser($scope.userForm) : usersService.updateUser($scope.userId, $scope.userForm);
                $scope.saving = true;
                promise.success(function(){
                    $scope.saving = false;
                }).error(function(data, status){
                        $log.log(data.error + ' ' + status);
                });
                $modalInstance.close(promise);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };


    };
    modalController.$inject = ['$scope', '$log', '$modalInstance', 'usersService', 'userId'];
    angular.module('usersApp').controller('modalController', modalController);
}());
